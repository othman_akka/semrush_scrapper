from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium import webdriver
from decimal import Decimal
import pandas as pd
import time
import re
import os
import glob

class Semrush():
    def __init__(self, email, password, link, browser='Chrome'):
        LOGIN_URL = 'https://www.semrush.com/login'

        self.email = email
        self.password = password
        self.link = link
        self.anchor1 = []
        self.anchor2 = []
        self.anchor3 = []
        self.ref = []
        self.year_2016 = []
        self.year_2017 = []
        self.year_2018 = []
        self.year_2019 = []
        self.year_2020 = []

        self.domains = []

        chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2,"download.default_directory":r"C:\Users\AA\PycharmProjects\semrush_scrapper\files",
                 "download.prompt_for_download": False,
                 "download.directory_upgrade": True,
                 "safebrowsing_for_trusted_sources_enabled": False,
                 "safebrowsing.enabled": False}
        chrome_options.add_experimental_option("prefs", prefs)

        if browser == 'Chrome':
            self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), chrome_options=chrome_options)
        elif browser == 'Firefox':
            self.driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
        self.driver.get(LOGIN_URL)
        time.sleep(2)

    def Login(self):
        email_element = self.driver.find_element_by_name('email')
        email_element.send_keys(self.email)

        password_element = self.driver.find_element_by_name('password')
        password_element.send_keys(self.password)

        login_button = self.driver.find_element_by_xpath("//button[@class='AuthForm__button--161NN ___SButton_1c7mz_gg_ __theme_1c7mz_gg_ __theme_primary-success_1c7mz_gg_ __size_1c7mz_gg_ __size_xl_1c7mz_gg_']")
        login_button.click()
        time.sleep(2)

    def Scraping(self, domain):
        try:
            self.domains.append(domain)
            self.driver.get(self.link)
            time.sleep(2)

            bar_search = WebDriverWait(self.driver, 100).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='___SValue_1yazh_gg_ __size_1yazh_gg_ __size_xl_1yazh_gg_']")))
            bar_search.send_keys(domain)

            search_button = self.driver.find_element_by_xpath(
                "//button[@class='___SButton_1c7mz_gg_ __theme_1c7mz_gg_ __theme_primary-success_1c7mz_gg_ __size_1c7mz_gg_ __size_xl_1c7mz_gg_']")
            search_button.click()
            time.sleep(2)

            self.driver.find_element_by_tag_name("body").send_keys(Keys.PAGE_DOWN)

            all_infos = WebDriverWait(self.driver, 100).until(
                EC.presence_of_element_located((By.XPATH, "//div[@class='report.module__reportCnt___1cf7L']")))

            ref = all_infos.find_element_by_xpath(
                ".//div[@class='summary.module__summary___zGau9 summary.module__theme__green___f_iKS']")
            ref = ref.find_element_by_xpath(".//div[@class='style.module__mainNumber___pseVS']")
            self.ref.append(ref.text)
            ##################################
            all_time = WebDriverWait(self.driver, 100).until(EC.presence_of_element_located(
                (By.XPATH, "//div[@class='style.module__header___ntjMA']/div[2]/div[2]/button[5]")))
            ActionChains(self.driver).move_to_element(all_time).click(all_time).perform()

            export = all_infos.find_element_by_xpath(
                ".//button[@class='___SButton_1c7mz_gg_ __theme_1c7mz_gg_ __theme_secondary-muted_1c7mz_gg_ __size_1c7mz_gg_ __size_m_1c7mz_gg_']")
            self.driver.implicitly_wait(10)
            ActionChains(self.driver).move_to_element(export).click(export).perform()
            time.sleep(10)
            export = self.driver.find_element_by_xpath("//div[@class='___SContainer_102p0_gg_']/div/div[2]")
            self.driver.implicitly_wait(10)
            ActionChains(self.driver).move_to_element(export).click(export).perform()
            time.sleep(50)
            print('okkkk')

            dataframe = pd.read_csv(glob.glob('files/*.csv')[0], sep=",").to_numpy()
            year_2016 = dataframe[:, 54:65]
            year_2017 = dataframe[:, 66:77]
            year_2018 = dataframe[:, 78:89]
            year_2019 = dataframe[:, 89:101]
            year_2020 = dataframe[:, 102:113]

            self.year_2016.append(self.sum_year(year_2016))
            self.year_2017.append(self.sum_year(year_2017))
            self.year_2018.append(self.sum_year(year_2018))
            self.year_2019.append(self.sum_year(year_2019))
            self.year_2020.append(self.sum_year(year_2020))

            print(self.year_2019)

            os.remove(glob.glob('files/*.csv')[0])

            ####################################

            self.driver.get("https://www.semrush.com/analytics/backlinks/anchors/?q=" + domain)
            time.sleep(3)
            table = self.driver.find_element_by_xpath("//table[@class='_table_b4zwn-yellow-team']/tbody")
            elements = table.find_elements_by_xpath(".//tr")
            cpt = 1
            for element in elements:
                if cpt == 1:
                    self.anchor1.append(element.find_element_by_xpath(".//td[1]").text)
                if cpt == 2:
                    self.anchor2.append(element.find_element_by_xpath(".//td[1]").text)
                if cpt == 3:
                    self.anchor3.append(element.find_element_by_xpath(".//td[1]").text)
                cpt = cpt + 1
            print('domains :',self.domains,' ref domains: ', self.ref, ' 2020 :', self.year_2020, ' 2019 :', self.year_2019, ' 2018 :', self.year_2018,
             ' 2017 :', self.year_2017, ' 2016 :', self.year_2016, " anchor1 :", self.anchor1, " anchor2 :", self.anchor2," anchor3 :", self.anchor3)
        except:
            print("problem in loading!")


    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True

    def sum_year(self, matrix):
        summ = 0
        for array in matrix:
            summ = summ +sum(array)
        return summ

    def Save_data(self):
        # create a csv file
        r = pd.DataFrame({'domains':self.domains,'ref domains': self.ref, '2020': self.year_2020, '2019': self.year_2019, '2018': self.year_2018,
             '2017': self.year_2017, '2016': self.year_2016, "anchor1": self.anchor1, "anchor2": self.anchor2,"anchor3": self.anchor3})
        r.to_csv("data.csv", index=True)








