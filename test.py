# -*- coding: utf-8 -*-
from flask import Flask, render_template, request, redirect, url_for, make_response
import os, csv, io
from os.path import join, dirname, realpath
from werkzeug.utils import secure_filename

import requests
import uuid
from pprint import pprint
import time
from bs4 import BeautifulSoup

app = Flask(__name__)

# enable debugging mode
app.config["DEBUG"] = False
app.config['UPLOAD_FOLDER'] = 'static'


def transform(text_file_contents):
    return text_file_contents.replace("=", ",")


# Root URL
@app.route('/')
def index():
    return render_template('index.tpl')


@app.route("/", methods=['POST'])
def uploadFiles():
    file = request.files['csv_file']
    filename = secure_filename(file.filename)

    file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)

    file.save(file_path)

    with io.open(file_path, encoding="ISO-8859-1") as f:
        file_content = (f.read())

    print(len(file_content.split("\n")))

    output = "domain,Ref domains,2020,2019,2018,2017,2016,anchor1,anchor2,anchor3,Google cache"
    for domain in file_content.split("\n")[1:]:
        res_dom = cheker(domain.strip().replace(
            "'latin-1' codec can't encode character '\ufeff' in position 165: Body ('\ufeff') is not valid Latin-1. Use body.encode('utf-8') if you want to send it encoded in UTF-8.",
            ""))
        print(domain.strip(), res_dom)
        print(res_dom)
        if res_dom:
            output = output + "\n{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}".format(domain, res_dom["ref-domains"],
                                                                                      res_dom["2020"], res_dom["2019"],
                                                                                      res_dom["2018"], res_dom["2017"],
                                                                                      res_dom["2016"],
                                                                                      res_dom["anchor1"],
                                                                                      res_dom["anchor2"],
                                                                                      res_dom["anchor3"],
                                                                                      res_dom["google_cache"])
        else:
            output = output + "\n{0},--,--,--,--,--,--,--,--,--,--".format(domain)

    print(output)
    response = make_response(output)
    response.headers["Content-Disposition"] = "attachment; filename=result.csv"
    response.headers["Content-type"] = "text/csv"
    return response


def semrush(data):
    try:
        # dcp=random.choice(open('proxy.txt').read().split("\n")[:-1])
        # proxy=dcp.split(":")
        # proxies = {'http': 'http://{0}:{1}'.format(proxy[0],proxy[1]),'https': 'http://{0}:{1}'.format(proxy[0],proxy[1])}
        # print(proxies)
        headers = {
            'authority': 'www.semrush.com',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36',
            'content-type': 'application/json; charset=utf-8'
        }
        response = requests.post('https://www.semrush.com/dpa/rpc', headers=headers, data=data, timeout=5)

        return response.json()["result"]

    except Exception as e:
        print(e)
        return False


def cheker(domain):
    try:

        data = '{"id":6,"jsonrpc":"2.0","method":"backlinks.Overview","params":{"request_id":"request_id_h","report":"domain.overview","args":{"searchItem":"searchItem_hash","searchType":"domain"},"userId":5615600,"apiKey":"721df89f2b9891dba83f9a7ff5b7a7d8"}}'.replace(
            "request_id_h", str(uuid.uuid4())).replace("searchItem_hash", domain)

        result = False
        while not result:
            result = semrush(data)

        site = {}
        site["ref-domains"] = result["domains"]
        anchors = result["anchors"]

        data = '{"id":14,"jsonrpc":"2.0","method":"organic.OverviewTrend","params":{"request_id":"request_id_h","report":"domain.overview","args":{"dateType":"monthly","searchItem":"searchItem_hash","searchType":"domain","dateRange":null,"database":"us","global":true},"userId":5615600,"apiKey":"721df89f2b9891dba83f9a7ff5b7a7d8"}}'.replace(
            "request_id_h", str(uuid.uuid4())).replace("searchItem_hash", domain)
        result = False
        while not result:
            result = semrush(data)

        site["2016"] = 0
        site["2017"] = 0
        site["2018"] = 0
        site["2019"] = 0
        site["2020"] = 0
        for res in result:
            if res["date"][:4] == '2016' and res["organicTraffic"] > site["2016"]:
                site["2016"] = res["organicTraffic"]
            if res["date"][:4] == '2017' and res["organicTraffic"] > site["2017"]:
                site["2017"] = res["organicTraffic"]
            if res["date"][:4] == '2018' and res["organicTraffic"] > site["2018"]:
                site["2018"] = res["organicTraffic"]
            if res["date"][:4] == '2019' and res["organicTraffic"] > site["2019"]:
                site["2019"] = res["organicTraffic"]
            if res["date"][:4] == '2020' and res["organicTraffic"] > site["2020"]:
                site["2020"] = res["organicTraffic"]

        headers = {
            'authority': 'indexchecking.com',
            'cache-control': 'max-age=0',
            'content-type': 'application/x-www-form-urlencoded',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36'
        }

        data = {
            'f_urls': str(domain)
        }

        response = requests.post('https://indexchecking.com/', headers=headers, data=data)

        soup = BeautifulSoup(response.text, "html.parser")
        index = soup.find("tr", class_="rowclass1").get_text().split(domain)[1]

        ####################################################################
        for i, anchor in enumerate(anchors[:3]):
            site["anchor{0}".format(i + 1)] = anchor["anchor"]

        if "anchor1" not in site:
            site["anchor1"] = "--"
        if "anchor2" not in site:
            site["anchor2"] = "--"
        if "anchor3" not in site:
            site["anchor3"] = "--"

        site["google_cache"] = index
        return site

    except Exception as e:
        print(e)
        return False


if (__name__ == "__main__"):
    app.run(host="192.168.43.3", port=5000)